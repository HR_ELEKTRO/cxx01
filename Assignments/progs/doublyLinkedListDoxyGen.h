#ifndef DOUBLY_LINKED_LIST_H_
#define DOUBLY_LINKED_LIST_H_
#include <stdbool.h>
#include <stddef.h>

 /**\struct DllNodeTag
 *  \brief This structure is used for DllNode
 *  \var DllNodeTag::data 
 *  Member 'data' contains pointer to the data
 *  \var DllNodeTag::next 
 *  Member 'next' contains pointer to the next node
 *  \var DllNodeTag::previous 
 *  Member 'previous' contains pointer to the previous node
 */
typedef struct DllNodeTag
{
    void *data;
    struct DllNodeTag *next;
    struct DllNodeTag *previous;
} DllNode;

 /**\struct DoublyLinkedList
 *  \brief This structure is used for DoublyLinkedList
 *  \var DoublyLinkedList::head 
 *  Member 'head' contains pointer to first node in list
 *  \var DoublyLinkedList::tail 
 *  Member 'tail' contains pointer to last node in list
 */
typedef struct
{
    DllNode *head ;
    DllNode *tail;
} DoublyLinkedList;

/**\brief To create an empty list.
 * \return location of the List just created, NULL if failed
 */
DoublyLinkedList *dllCreate(void);

/**\brief To delete the list.
 * \return void, the list is deleted.
 */
void dllDelete(DoublyLinkedList **ptrTheList);

/**\brief adding data in front of the list
 * \param theList List where the data is put to front
 * \param theData Pointer to the data
 * \return DllNode The newly crated node, or when failing: NULL
 */DllNode *dllAddBeforeHead(DoublyLinkedList *theList, void *theData);

/**\brief adding data to the back of the list
 * \param theList List where the data is put in the back
 * \param theData Pointer to the data
 * \return DllNode The newly crated node, or when failing: NULL
 */
DllNode *dllAddAfterTail(DoublyLinkedList *theList, void *theData);

/**\brief adding data before given node
 * \param theList List where node is availeble
 * \param theNode Node where data should be put before
 * \param theData Pointer to the data
 * \return DllNode The newly crated node, or when failing: NULL
 */
DllNode *dllAddBefore(DoublyLinkedList *theList, DllNode *theNode, void *theData);

/**\brief adding data after given node
 * \param theList List where node is availeble
 * \param theNode Node where data should be put after
 * \param theData Pointer to the data
 * \return DllNode The newly crated node, or when failing: NULL
 */
 DllNode *dllAddAfter(DoublyLinkedList *theList, DllNode *theNode, void *theData);

/**\brief delete a single node from the list
 * \param theList List the node belonges to
 * \param theNode The node which is to be deleted
 * \return void, the node is deleted
 */
void dllDeleteNode(DoublyLinkedList *theList, DllNode* theNode);

/**\brief searching for first node which meet the requerements from predicate function
 * \param theList List where node should be availeble 
 * \param predicate function to look through the list
 * \param theData pointer to data to be used by predicate
 * \return DllNode The node which met requerement(s) of predicate, NULL when no node is found
 */
DllNode *dllFindFirst(DoublyLinkedList *theList, bool (*predicate)(void *d1, void *d2), void *theData);

/**\brief searching for last node which meet the requerements from predicate function
 * \param theList List where node should be availeble 
 * \param predicate function to look through the list
 * \param theData pointer to data to be used by predicate
 * \return DllNode The node which met requerement(s) of predicate, NULL when no node is found
 */
DllNode *dllFindLast(DoublyLinkedList *theList, bool (*predicate)(void *d1, void *d2), void *theData);

/**\brief searching for first node which meet the requerements from predicate function after given node
 * \param theList List where node should be availeble 
 * \param predicate function to look through the list
 * \param theData pointer to data to be used by predicate
 * \return DllNode The node which met requerement(s) of predicate, NULL when no node is found
 */
DllNode *dllFindAfter(DoublyLinkedList *theList, DllNode *theNode, bool (*predicate)(void *d1, void *d2), void *theData);

/**\brief searching for last node which meet the requerements from predicate function before given node
 * \param theList List where node should be availeble 
 * \param predicate function to look through the list
 * \param theData pointer to data to be used by predicate
 * \return DllNode The node which met requerement(s) of predicate, NULL when no node is found
 */
DllNode *dllFindBefore(DoublyLinkedList *theList, DllNode *theNode, bool (*predicate)(void *d1, void *d2), void *theData);

/**\brief gets number of elements in the list
 * \param theList List to get the amount of nodes
 * \return size_t amount of nodes
 */
size_t dllNumberOfElements(DoublyLinkedList *theList);

#endif
