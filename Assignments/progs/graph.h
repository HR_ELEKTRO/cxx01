#ifndef GRAPH_H_
#define GRAPH_H_
#include "doublyLinkedList.h"

typedef struct 
{ 
    char* name; 
    void* data;
    DoublyLinkedList* edges; 
} Vertex;

typedef struct 
{ 
    Vertex* destination; 
    int weight; 
} Edge;

typedef struct 
{
    DoublyLinkedList* vertices;
} Graph;

// declare the prototypes of your functions here 

#endif
