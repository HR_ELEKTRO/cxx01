#ifndef SINGLY_LINKED_LIST_H_
#define SINGLY_LINKED_LIST_H_

typedef struct PhonemeNodeTag
{
    char *phoneme;
    struct PhonemeNodeTag *next;
} PhonemeNode;

void playWord(PhonemeNode *phonemeNodePtr);
void freeWord(PhonemeNode **phonemeNodePtrPtr);

#endif