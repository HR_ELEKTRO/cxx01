#include <stdio.h>
#include <limits.h>

#include "breuk.h"
#include "munit/munit.h"

// Test functions always return MunitResult
static MunitResult test_Add(const MunitParameter params[], void* user_data)
{
    // These are just to silence compiler warnings about the parameters being unused. 
    (void) params;
    (void) user_data;

    // Init
    Breuk a = {1, 2}, b = {1, 4};

    // Add 
    Breuk c = add(a, b);
    
    // Check results
    munit_assert_int(c.teller, ==, 3);
    munit_assert_int(c.noemer, ==, 4);

    return MUNIT_OK;
}

static MunitResult test_Mul(const MunitParameter params[], void* user_data)
{
    (void) params;
    (void) user_data;

    // Init
    Breuk a = {1, 2}, b = {1, 4};

    // Multiply
    Breuk c = multiply(a, b);

    // Check results
    munit_assert_int(c.teller, ==, 1);
    munit_assert_int(c.noemer, ==, 8);

    return MUNIT_OK;
}

static MunitResult test_Zero(const MunitParameter params[], void* user_data)
{
    (void) params;
    (void) user_data;

    // Init
    Breuk a = { 0, 1}, b = {3, 1};

    // Add
    Breuk c = add(a, b);
    
    // Check results
    munit_assert_int(c.teller, ==, 3);
    munit_assert_int(c.noemer, ==, 1);

    // Multiply
    Breuk d = multiply(a, b);

    // Check results
    munit_assert_int(d.teller, ==, 0);
    munit_assert_int(d.noemer, ==, 1);

    return MUNIT_OK;
}

// A test suite is a bunch of tests combined together
static MunitTest test_suite_tests[] = {
    { (char*) "/Add", test_Add, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL },
    { (char*) "/Multiply", test_Mul, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL },
    { (char*) "/Zero", test_Zero, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL },
    // Always add this one to tell runner testing is over!
    { NULL, NULL, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL }
};

// Main runner that picks which suites we should run
static const MunitSuite test_suite = {
    (char*) "test Breuk",
    test_suite_tests,
    NULL,
    1,
    MUNIT_SUITE_OPTION_NONE
};

int main(int argc, char* argv[MUNIT_ARRAY_PARAM(argc + 1)])
{
    return munit_suite_main(&test_suite, NULL, argc, argv);
}

// As an exercise you can add more tests

