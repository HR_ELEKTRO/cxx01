#ifndef Breuk_H_
#define Breuk_H_

typedef struct
{
    int teller;
    int noemer;
} Breuk;

Breuk add(Breuk b1, Breuk b2);
Breuk multiply(Breuk b1, Breuk b2);

#endif
