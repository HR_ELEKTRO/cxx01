#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

// BroJZ 2016 - modified by BaRoy 2018 - modified by BroJZ 2019
// This example shows the implementation of a flexible and generic find function.
// Which is implemented by using a function pointer parameter to pass the predicate.

// In this example the data structure used is a generic array of void pointers.
// The last valid element in the array must be 0 (zero).

void *findFirst(void *theArray[], bool (*predicate)(void *d1, void *d2), void *theData)
{
    for (size_t i = 0; theArray[i] != 0; i++)
    {
        if ((*predicate)(theArray[i], theData))
        {
            return theArray[i];
        }
    }
    return 0;
}

// predicate to find int < value
bool intLessThan(void *theInt, void *value)
{
    assert(theInt != 0 && value != 0);
    return *(int *)theInt < *(int *)value;
}

// predicate to find int > value
bool intGreaterThan(void *theInt, void *value)
{
    assert(theInt != 0 && value != 0);
    return *(int *)theInt > *(int *)value;
}

// predicate to find int == value
bool intEqualTo(void *theInt, void *value)
{
    assert(theInt != 0 && value != 0);
    return *(int *)theInt == *(int *)value;
}

// struct for the between function
typedef struct {
  int low;
  int high;
} range;

// predicate to find int in range [data->low, data->high]
bool intInRange(void *theInt, void *data)
{
    assert(theInt != 0 && data != 0);
    range *r = (range*)data;
    int i = *(int *)theInt;
    return i >= r->low && i <= r->high;
}

// predicate to find double > value
bool doubleGreaterThan(void *theDouble, void *value)
{
    assert(theDouble != 0 && value != 0);
    return *(double *)theDouble > *(double *)value;
}

// some handy functions:
int *newInt(int value)
{
    int *p = malloc(sizeof(int));
    assert(p != 0);
    *p = value;
    return p;
}

double *newDouble(double value)
{
    double *p = malloc(sizeof(double));
    assert(p != 0);
    *p = value;
    return p;
}

void printInt(void *data)
{
    assert(data != 0);
    printf("%d", *(int *)data);
}

void printDouble(void *data)
{
    assert(data != 0);
    printf("%g", *(double *)data);
}

void printGenericArray(void *theArray[], char *name, void (*print)(void *d))
{
    assert(name != 0 && print != 0);
    printf("%s: ", name);
    if (theArray[0] != 0)
    {
        print(theArray[0]);
    }
    for (size_t i = 1; theArray[i] != 0; i++)
    {
        printf(", ");
        print(theArray[i]);
    }
    printf("\n");
}

int main(void)
{
    void *a1[] = {newInt(17), newInt(-2), newInt(13), newInt(42), 0};
    void *a2[] = {newDouble(-0.23), newDouble(3.1415), newDouble(1E18), newDouble(1E-18), 0};

    printGenericArray(a1, "a1", printInt);
    printGenericArray(a2, "a2", printDouble);

    int ci = 0;
    int *pi = findFirst(a1, &intLessThan, &ci);
    if (pi == 0)
    {
        printf("No value found in a1 which is < %d.\n", ci);
    }
    else
    {
        printf("The first value found in a1 which is < %d is %d.\n", ci, *pi);
    }

    ci = 100;
    pi = findFirst(a1, &intGreaterThan, (void*)&ci);
    if (pi == 0)
    {
        printf("No value found in a1 which is > %d.\n", ci);
    }
    else
    {
        printf("The first value found in a1 which is > %d is %d.\n", ci, *pi);
    }

    ci = 13;
    pi = findFirst(a1, &intEqualTo, &ci);
    if (pi == 0)
    {
        printf("No value found in a1 which is equal to %d.\n", ci);
    }
    else
    {
        printf("The first value found in a1 which is equal to %d is %d.\n", ci, *pi);
    }

    range r = {10, 15};
    pi = findFirst(a1, &intInRange, &r);
    if (pi == 0)
    {
        printf("No value found in a1 which is between %d and %d.\n", r.low, r.high);
    }
    else
    {
        printf("The first value found in a1 which is in range [%d, %d] is %d.\n", r.low, r.high, *pi);
    }


    double cd = 2.5;
    double *pd = findFirst(a2, &doubleGreaterThan, &cd);
    if (pd == 0)
    {
        printf("No value found in a2 which is > %f.\n", cd);
    }
    else
    {
        printf("The first value found in a2 which is > %f is %f.\n", cd, *pd);
    }

    return 0;
}

