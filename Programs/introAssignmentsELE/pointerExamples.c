/*
* File: pointerExamples.c
* Author: Remco de Wit, Harry Broeders
* Version: 1.2
* Description: A program to show how pointers work.
* some uses of pointers are illustrated in this file.
*/
#include <stdlib.h>
#include <stdio.h>

int main(void)
{
    int a, b;
    int *p;

    a = 5;
    b = 42;
    p = &a;

    printf("The value of a is: %d\n", a);
    printf("The memory location where a is stored is: %p\n", &a);
    printf("The value of b is: %d\n", b);
    printf("The memory location where b is stored is: %p\n", &b);
    printf("Pointer p points to memory location: %p\n", p);
    printf("The value stored at the location pointed to by p is: %d\n", *p);
    printf("Pointer p itself is stored in memory location: %p\n", &p);

    printf("\nIf p points to a then a can be changed by writing to *p\n");
    *p = 10;
    printf("The value of a is: %d\n", a);
    printf("The memory location where a is stored is: %p\n", &a);
    printf("The value of b is: %d\n", b);
    printf("The memory location where b is stored is: %p\n", &b);
    printf("Pointer p points to memory location: %p\n", p);
    printf("The value stored at the location pointed to by p is: %d\n", *p);
    printf("Pointer p itself is stored in memory location: %p\n", &p);

    printf("\nIf we write to p we can change the variable to which p refers.\n");
    p = &b;
    printf("The value of a is: %d\n", a);
    printf("The memory location where a is stored is: %p\n", &a);
    printf("The value of b is: %d\n", b);
    printf("The memory location where b is stored is: %p\n", &b);
    printf("Pointer p points to memory location: %p\n", p);
    printf("The value stored at the location pointed to by p is: %d\n", *p);
    printf("Pointer p itself is stored in memory location: %p\n", &p);

    return 0;
}
