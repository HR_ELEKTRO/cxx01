/*
* File: pointers1.c
* Author:
* Version:
* Description:
*/

#include <stdio.h>

int main(void)
{
    int a, b, c;
    int *d, *e;

    a = 5;
    b = 6;
    c = 10;

    // Print the value and the addresses of variable a, b, and c by only using pointer d in the printf statements.


    // You can also do arithmetic operations using pointers.
    // It works just as normal variables, just remember to use the dereference *
    d = &a;
    c = b + *d;

    // Now calculate c = a * b using pointers d and e and print the result.

    return 0;
}
