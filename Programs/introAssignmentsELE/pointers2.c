/*
* File: pointers2.c
* Author:
* Version:
* Description:
*/

#include <stdio.h>

int main(void)
{
    int a[10];
    int *b;

    int i;

    // Fill array a with ascending values
    for(i = 0; i < 10; i++)
    {
        a[i] = i;
    }

    // Pointers can be used to index arrays
    b = a; // This statement is the same as b = &a[0]
    // b will point to the first element of array a.
    // with b++ you can step through the array.
    // print the entire array using pointer b


    // Now print the entire array once again, but with each element incremented by 1.
    // Again only use pointer b.

    // Now adjust the program to reverse the array using two pointers
    // (you may use one int variable for swapping to values in the array)


    // Print the reversed array


    return 0;
}
