/*
* File: pointers3.c
* Author:
* Version:
* Description:
*/

#include <stdio.h>

// function prototypes
void swap(int *a, int *b);

int main(void)
{
    int a, b;

    a = 5;
    b = 10;

    printf("The value of a was %d\nThe value of b was %d\n", a, b);

    // now you must swap the values of a and b using the function swap
    swap(/* fill in the proper arguments yourself */);

    printf("The value of a is %d\nThe value of b is %d\n", a, b);

    return 0;
}

// This function must swap the values of variable pointed to by p1
// with the variable pointed to by p2, without returning any values.
void swap(int *p1, int *p2)
{



}
