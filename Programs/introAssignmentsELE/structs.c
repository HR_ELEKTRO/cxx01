#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/*
* A struct is a collection of data.
* For this exercise we implement a struct with an integer "numberOfPhonemes"
* and an array of char *'s "phonemes".
* We have implemented a typedef for this struct named Word.
*/
typedef struct
{
    int numberOfPhonemes;
    char *phonemes[20];
} Word;

/*
* This function plays a single phoneme. It uses the build-in audio player of Linux.
* NOTE for Arch Linux see: https://wiki.archlinux.org/index.php/Advanced_Linux_Sound_Architecture
* Note that this function is blocking (i.e. your program will not continue until
* Linux has finished playing the audio file).
* The phonemes must be placed in the directory named Phonemes which must be located in the
* same directory as the executable.
* The North American English phonemes can be downloaded from: 
* https://bitbucket.org/HR_ELEKTRO/cxx01/downloads/Phonemes.zip
* This files include audio files for the phonemes:
* "AA", "AE", "AH", "AO", "AW", "AX", "AY", "B", "CH", "D", "DH", "EH", "ER", "EY",
* "F", "G", "HH", "IH", "IY", "J", "K", "L", "M", "N", "NG", "OW", "OY", "P", "R", 
* "S", "SH", "T", "TH", "UH", "UW", "V", "W", "WH", "Y", "YU", "Z", "ZH"
*/
void playPhoneme(char *phoneme)
{
    char command[64];
    // create command to execute playback
    sprintf(command, "aplay -c 1 -q -t au Phonemes/%s.snd", phoneme);
    // play soundfile for phoneme
    if (system(command) != 0)
    {
        printf("Error while trying to execute command: %s\n", command);
    }
}

/*
* This function plays a word. The function requires a pointer to a Word struct defined above.
*/
void playWord(Word *pw)
{
    for (int i = 0; i < pw->numberOfPhonemes; i++)
    {
        playPhoneme(pw->phonemes[i]);
    }
}

/*
* Build a program using the struct and the playWord function to playback a sentence which
* consists of several words.
* Use a proper delay between the words. 
* You can use the CMU Pronouncing Dictionary to break an American English word into phonemes:
* http://www.speech.cs.cmu.edu/cgi-bin/cmudict
*/
int main(void)
{
    /*
    * Declare the Word structs here.
    * Fill the structs with data (the numberOfPhonemes and the phonemes).
    * Pass a pointer to each Word struct to the playWord() function to synthesize each word.
    * Insert a proper delay between the words.
    */

    return 0;
}
