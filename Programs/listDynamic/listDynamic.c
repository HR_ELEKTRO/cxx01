#include <stdio.h>
#include <stdlib.h>

typedef struct ListNodeTag {
	//datavelden
	int value;
	//link
	struct ListNodeTag *next;
} ListNode;

void printList(ListNode* n)
{
    while (n != 0)
    {
        printf("%d", n->value);
        if (n->next != 0)
        {
            printf(" --> ");
        }
        n = n->next;
    }
    printf("\n");
}

int main(void)
{
    ListNode *head = 0, *tail = 0;
    int aantalNodes;

    printf("Hoeveel nodes wenst u? ");
    scanf("%d", &aantalNodes);

    for (int n = 0; n < aantalNodes; n++)
    {
        ListNode *newNode = malloc(sizeof(ListNode));
        newNode->value = n;
        newNode->next = 0;
        if (head == 0)
        {
            head = tail = newNode;
        }
        else
        {
            tail = tail->next = newNode;
        }
    }

    printList(head);

    // freelist
    while (head != 0)
    {
        ListNode *node = head;
        head = head -> next;
        free(node);
    }
    tail = 0;

    return 0;
}
