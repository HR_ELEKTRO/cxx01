#include <stdio.h>

typedef struct ListNodeTag {
	//datavelden
	int value;
	//link
	struct ListNodeTag *next;
} ListNode;

void printList(ListNode* n)
{
    while (n != 0)
    {
        printf("%d", n->value);
        if (n->next != 0)
        {
            printf(" --> ");
        }
        n = n->next;
    }
    printf("\n");
}

int main(void)
{
    ListNode a, b, c;
    a.next = b.next = c.next = 0;
    a.value = 1; b.value = 2; c.value = 3;

    ListNode* head = &a;
    a.next = &b;
    b.next = &c;

    printList(head);

    return 0;
}
