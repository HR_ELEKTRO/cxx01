#include <stdio.h>
#include <stdbool.h>
#include "read_json_graph.h"

void printNodes(nodeInfo *n)
{
    if (n->next != 0) printNodes(n->next);
    else printf("Nodes:\n");
    printf("id = %d", n->id);
    printf(", label = %s", n->label);
    if (n->hasColor) printf(", color = #%06X", n->color);
    printf("\n");
}

void printEdges(edgeInfo *e)
{
    if (e->next != 0) printEdges(e->next);
    else printf("Edges:\n");
    printf("from = %d", e->from);
    printf(", to = %d", e->to);
    if (e->hasWeight) printf(", weight = %d", e->weight);
    if (e->hasColor) printf(", color = #%06X", e->color);
    printf("\n");
}

parseError testParse(char *filename)
{
    printf("\nParsing %s\n", filename);
    graphInfo gi;
    parseError res = parse(filename, &gi);
    // You have to create your own graph here using variable gi.
    // In this program the graph information is only printed.
    printf("directed = %d\n", gi.isDirected);
    printf("weighted = %d\n", gi.isWeighted);
    if (gi.nodeStack != 0) printNodes(gi.nodeStack);
    if (gi.edgeStack != 0) printEdges(gi.edgeStack);
    // Free the memory allocated by the parser by calling freeGraphInfo.
    freeGraphInfo(&gi);
    return res;
}

int main(void)
{
    int res = 0;
    res += testParse("../../Assignments/progs/circleOfThree.json");
    res += testParse("../../Assignments/progs/circleOfThreeSolved.json");
    res += testParse("../../Assignments/progs/cities.json");
    res += testParse("../../Assignments/progs/citiesColor.json");
    res += testParse("../../Assignments/progs/citiesShortestPath.json");
    res += testParse("../../Assignments/progs/provincesNetherlands.json");
    res += testParse("../../Assignments/progs/weightedDiGraph.json");
    res += testParse("testUnweightedColored.json");
    res += testParse("testUnweightedColoredXY.json");
    res += testParse("citiesColorXY.json");
    res += testParse("colorNULL.json");
    res += testParse("testNegativeWeighted.json");
    return res;
}
