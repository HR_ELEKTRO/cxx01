==18360== Memcheck, a memory error detector
==18360== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==18360== Using Valgrind-3.14.0 and LibVEX; rerun with -h for copyright info
==18360== Command: ./a.out
==18360==
==18360== Invalid write of size 4
==18360==    at 0x109153: main (stupid.c:5)
==18360==  Address 0x4a1c068 is 0 bytes after a block of size 40 alloc'd
==18360==    at 0x483577F: malloc (in /usr/lib/x86_64-linux-gnu/valgrind/vgpreload_memcheck-amd64-linux.so)
==18360==    by 0x109146: main (stupid.c:4)
==18360==
==18360==
==18360== HEAP SUMMARY:
==18360==     in use at exit: 40 bytes in 1 blocks
==18360==   total heap usage: 1 allocs, 0 frees, 40 bytes allocated
==18360==
==18360== 40 bytes in 1 blocks are definitely lost in loss record 1 of 1
==18360==    at 0x483577F: malloc (in /usr/lib/x86_64-linux-gnu/valgrind/vgpreload_memcheck-amd64-linux.so)
==18360==    by 0x109146: main (stupid.c:4)
==18360==
==18360== LEAK SUMMARY:
==18360==    definitely lost: 40 bytes in 1 blocks
==18360==    indirectly lost: 0 bytes in 0 blocks
==18360==      possibly lost: 0 bytes in 0 blocks
==18360==    still reachable: 0 bytes in 0 blocks
==18360==         suppressed: 0 bytes in 0 blocks
==18360==
==18360== For counts of detected and suppressed errors, rerun with: -v
==18360== ERROR SUMMARY: 2 errors from 2 contexts (suppressed: 0 from 0)

