# Advanced C Programming #

In dit repository zijn de Modulewijzer en de Assignments opgenomen die bij de module CXX01 (Advanced C Programming) van de minor Embedded Systems van de Hogeschool Rotterdam gebruikt worden.

**Deze module wordt niet meer aangeboden en is voor het laatst in het studiejaar 2019-2020 gegeven.**

Je kunt de gecompileerde versies (in pdf) vinden de [Wiki](https://bitbucket.org/HR_ELEKTRO/cxx01/wiki) van dit repository.

Op- en aanmerkingen zijn altijd welkom. Maak een [issue](https://bitbucket.org/HR_ELEKTRO/cxx01/issues?status=new&status=open) aan of stuur een mail naar [Harry Broeders](mailto:j.z.m.broeders@hr.nl).