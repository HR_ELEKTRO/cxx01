#include <stdio.h>
#include "tree.h"

int main(void)
{
    tree_node *root = create_expression(
        '+',
        create_number_node(12),
        create_expression(
            '*',
            create_expression(
                '/',
                create_number_node(40),
                create_expression(
                    '-',
                    create_number_node(23),
                    create_number_node(3)
                )
            ),
            create_number_node(2)
        )
    );

    printf("Infix:   ");
    print_tree_infix(root);
    printf("\n");

    printf("Postfix: ");
    print_tree_postfix(root);
    printf("\n");

    FILE *file = fopen("tree.graphviz", "w");
    if (file == NULL)
    {
        fprintf(stderr, "Error: Can not create file tree.graphviz.");
        return -1;
    }

    create_visual_graph_from_tree(root, file);
    printf("Open file tree.graphviz op https://stamm-wilbrandt.de/GraphvizFiddle/#.");

    free_expression_tree(&root);

    return 0;
}
