#include <stdbool.h>
// Datastructure Stack

// Structs needed to build a generic and dynamic Stack:

typedef struct StackElementTag
{
    void *data;
    struct StackElementTag *next;
} StackElement;

typedef struct
{
    StackElement *top;
} Stack;

// Functions supported by a dynamic Stack:

Stack *createStack(void); // Creates and returns an empty stack
void removeStack(Stack **s); // Removes the stack s from memory and sets *s to NULL.
void push(Stack *s, void *data); // Adds the element void *data on top of the stack s
void pop(Stack *s); // Removes the element currently on top of the stack s
void *top(const Stack *s); // Returns the void *data of the element currently on top of the stack s
bool empty(const Stack *s); // Returns true if the stack s is empty
