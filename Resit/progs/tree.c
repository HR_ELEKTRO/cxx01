#include <stdio.h>
#include <stdlib.h>
#include "tree.h"

tree_node *create_number_node(int i)
{
    // Dynamically reserve memory for a tree_node
    // of type number_type.
    // Set the_number to i and return the tree_node
    return NULL; // mock up implementation
}

tree_node *create_expression(char op, tree_node *l, tree_node *r)
{
    // Dynamically reserve memory for a tree_node
    // of type operator_type.
    // Set the_operator to op, left_operand to l and right_operand to r
    // and return the tree_node
    return NULL; // mock up implementation
}

void free_expression_tree(tree_node **pnode)
{
    // Free all dynamically reserved memory
    // and set *pnode to NULL.
}

void print_tree_postfix(const tree_node *node)
{
    // Print the expression tree in postfix order
    // https://en.wikipedia.org/wiki/Binary_expression_tree#Postfix_traversal
}

void print_tree_infix(const tree_node *node)
{
    // Print the expression tree in postfix order
    // https://en.wikipedia.org/wiki/Binary_expression_tree#Infix_traversal
}

static void output_edge_to_visual_graph(char source, const tree_node *dest, FILE* file)
{
    fprintf(file, "  \"%c\" -> \"", source);
    switch (dest->tree_node_type)
    {
        case operator_type:
            fprintf(file, "%c", dest->the_operator_node.the_operator);
            break;
        case number_type:
            fprintf(file, "%d", dest->the_number);
            break;
    }
    fprintf(file, "\"\n");
}

static void output_node_to_visual_graph(const tree_node *node, FILE *file)
{
    if (node != NULL && node->tree_node_type == operator_type)
    {
        const operator_node *op = &(node->the_operator_node);
        if (op->left_operand != NULL && op->right_operand != NULL)
        {
            output_edge_to_visual_graph(op->the_operator, op->left_operand, file);
            output_edge_to_visual_graph(op->the_operator, op->right_operand, file);
            output_node_to_visual_graph(op->left_operand, file);
            output_node_to_visual_graph(op->right_operand, file);
        }
    }
}

void create_visual_graph_from_tree(const tree_node *root, FILE *file)
{
    if (file != NULL)
    {
        fprintf(file, "digraph G {\n");
        if (root != NULL) output_node_to_visual_graph(root, file);
        fprintf(file, "}\n");
    }
}
