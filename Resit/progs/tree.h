#ifndef __BroJZ_TREE__
#define __BroJZ_TREE__

typedef struct
{
    char the_operator;
    struct operand_node_tag *left_operand;
    struct operand_node_tag *right_operand;
} operator_node;

typedef struct operand_node_tag
{
    enum {operator_type, number_type} tree_node_type;
    union
    {
        operator_node the_operator_node;
        int the_number;
    };
} tree_node;

tree_node *create_number_node(int i);
tree_node *create_expression(char op, tree_node *l, tree_node *r);
void free_expression_tree(tree_node **pnode);
// frees all dynamically reserved memory and sets *pnode to NULL.

void print_tree_infix(const tree_node *node);
void print_tree_postfix(const tree_node *node);
void create_visual_graph_from_tree(const tree_node *root, FILE *file);

#endif
